
![Scheme](images/logo.JPG)

Story of image

Cactus tree is a well adapted tree for barren area of landscapes. It adapts for it surroundings. Likewise, ARPDroid  can be used to adapt your applications for android runtime permission mechanism without opening your sourcecode.  

### What is this repository for? ###
This tool is to detects the compatibility issues in a given app (old apps) with Android M's runtime permission mechanism and fix them when found, hence automatically adapting the app to the run-time permission mechanism.

For more information please check https://bitbucket.org/malindadoo/arpdroid/wiki/Home

For any other information (Not in wiki) you can contact - malinda.dilhara@gmail.com
