package Artifacts;

/**
 * Created by malindam on 8/13/2017.
 */
public final class Details {
    public static final String FOLDER_PATH = "D:\\WSU_Project2\\SOOT\\apks\\";
    public static final String APK = "yogi.corporationapps.telescope.bigzoomhd.apk";
    public static final String APK_PATH = FOLDER_PATH + "\\" + APK;
    public static final String PERMISSIONFILE = "AutoGrantedPermissions.txt";
    public static final String ANDROID_PLATFORMS = "D:\\WSU_Project2\\android-platforms-master";
    public static final String PERMISSION_MAPPINGS = "D:\\WSU_Project2\\SOOT\\APKDroid\\src\\main\\resources\\PermissionAPImapping\\results";
    public static final String SOOT_DUMMY_METHOD = "<dummyMainClass: void dummyMainMethod(java.lang.String[])>";
}
